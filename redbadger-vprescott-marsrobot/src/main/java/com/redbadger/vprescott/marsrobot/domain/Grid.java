package com.redbadger.vprescott.marsrobot.domain;

import com.redbadger.vprescott.marsrobot.enums.Movement;
import com.redbadger.vprescott.marsrobot.exceptions.InvalidGridSizeException;

import java.util.ArrayList;
import java.util.List;

public class Grid {

    /*
    Constants: the allowed grid dimensions
     */

    private static final int MIN_WIDTH = 1;
    private static final int MIN_HEIGHT = 1;
    private static final int MAX_WIDTH = 50;
    private static final int MAX_HEIGHT = 50;

    private int width;
    private int height;

    private List<Scent> scents = new ArrayList<Scent>();

    /*
    Constructor: the width and the height must be passed at this point
    as it won't be allowed to change them later
     */

    public Grid (int width, int height) throws InvalidGridSizeException {
        this.width = width;
        this.height = height;

        if (
                (this.width < MIN_WIDTH)
                ||
                (this.width > MAX_WIDTH)
                ||
                (this.height < MIN_HEIGHT)
                ||
                (this.height > MAX_HEIGHT)
                ) {

            throw new InvalidGridSizeException("Invalid grid size");
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public List<Scent> getScents() {
        return scents;
    }

    public void setScents(List<Scent> scents) {
        this.scents = scents;
    }

    public void addScent(Scent scent) {
        this.scents.add(scent);
    }

    public boolean isValidPositionOnGrid (Position position) {

        if (
                (position.getX() >= 1)
                &&
                (position.getX() <= this.getWidth())
                &&
                (position.getY() >= 1)
                &&
                (position.getY() <= this.getHeight())
                ) {

            return true;
        }
        else {
            return false;
        }
    }

    public boolean hasScentOnThisMove (Position position, Movement movement) {

        Scent givenScent = new Scent (position, movement);

        for (Scent scent : scents) {
            if (givenScent.equals(scent)) {
                return true;
            }
        }

        return false;
    }

    public static int getMinWidth() {
        return MIN_WIDTH;
    }

    public static int getMinHeight() {
        return MIN_HEIGHT;
    }

    public static int getMaxWidth() {
        return MAX_WIDTH;
    }

    public static int getMaxHeight() {
        return MAX_HEIGHT;
    }
}
