package com.redbadger.vprescott.marsrobot.domain;

import com.redbadger.vprescott.marsrobot.enums.Movement;

public class Scent {

    private Position position;
    private Movement movement;

    public Scent (Position position, Movement movement) {
        this.position = position;
        this.movement = movement;
    }

    public Position getPosition() {
        return position;
    }

    public Movement getMovement() {
        return movement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Scent scent = (Scent) o;

        if (!position.equals(scent.position)) return false;
        return movement == scent.movement;

    }

    @Override
    public int hashCode() {
        int result = position.hashCode();
        result = 31 * result + movement.hashCode();
        return result;
    }
}
