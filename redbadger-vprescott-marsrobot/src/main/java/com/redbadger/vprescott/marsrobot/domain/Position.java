package com.redbadger.vprescott.marsrobot.domain;

import com.redbadger.vprescott.marsrobot.enums.Orientation;

public class Position {

    private static int DEFAULT_X = 1;
    private static int DEFAULT_Y = 1;
    private static Orientation DEFAULT_ORIENTATION = Orientation.E;

    private int x;
    private int y;
    private Orientation orientation;

    public Position(int x, int y, Orientation orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    public Position(int x, int y) {
        this (x, y, DEFAULT_ORIENTATION);
    }

    public Position() {
        this (DEFAULT_X, DEFAULT_Y, DEFAULT_ORIENTATION);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (x != position.x) return false;
        if (y != position.y) return false;
        return orientation == position.orientation;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + orientation.hashCode();
        return result;
    }
}