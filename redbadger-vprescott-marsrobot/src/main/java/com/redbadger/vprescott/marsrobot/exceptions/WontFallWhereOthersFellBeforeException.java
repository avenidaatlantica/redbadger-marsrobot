package com.redbadger.vprescott.marsrobot.exceptions;

public class WontFallWhereOthersFellBeforeException extends Exception {

    public WontFallWhereOthersFellBeforeException(String message) {
        super(message);
    }

    public WontFallWhereOthersFellBeforeException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
