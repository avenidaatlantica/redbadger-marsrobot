package com.redbadger.vprescott.marsrobot.exceptions;

public class InvalidGridSizeException extends Exception {

    public InvalidGridSizeException(String message) {
        super(message);
    }

    public InvalidGridSizeException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
