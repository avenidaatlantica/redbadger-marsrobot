package com.redbadger.vprescott.marsrobot.domain;


import com.redbadger.vprescott.marsrobot.enums.Movement;
import com.redbadger.vprescott.marsrobot.enums.Orientation;
import com.redbadger.vprescott.marsrobot.exceptions.FellOffTheGridException;
import com.redbadger.vprescott.marsrobot.exceptions.NullGridException;
import com.redbadger.vprescott.marsrobot.exceptions.NullPositionException;
import com.redbadger.vprescott.marsrobot.exceptions.WontFallWhereOthersFellBeforeException;

public class Robot {

    private Grid grid;
    private Position position;

    private Position wouldHaveBeenPosition;

    private boolean fallenOffTheGrid = false;

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
        this.position = new Position();
    }

    public Position getPosition() {
        return position;
    }

    public Position getWouldHaveBeenPosition() {
        return wouldHaveBeenPosition;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setPosition(int x, int y) {
        this.position = new Position (x, y);
    }

    public void setPosition(int x, int y, Orientation orientation) {
        this.position = new Position (x, y, orientation);
    }

    public boolean isFallenOffTheGrid() {
        return fallenOffTheGrid;
    }

    /*
    Movement instructions
     */

    public void move(Movement movement)
            throws NullGridException,
            NullPositionException,
            FellOffTheGridException,
            WontFallWhereOthersFellBeforeException {

        if (this.getGrid() == null) {
            throw new NullGridException("The grid is not set");
        }

        if (this.getPosition() == null) {
            throw new NullPositionException("The position is not set");
        }

        if (this.getGrid().hasScentOnThisMove(this.getPosition(), movement)) {
            throw new WontFallWhereOthersFellBeforeException("Won't fall where others fell before");
        }

        Position lastKnownPosition = this.getPosition();

        if (movement.equals (Movement.F)) {
            this.setPosition(
                this.getPosition().getOrientation().equals(Orientation.N) ?
                new Position ((this.getPosition().getX()), (this.getPosition().getY() + 1), this.getPosition().getOrientation()) :
                this.getPosition().getOrientation().equals(Orientation.E) ?
                new Position ((this.getPosition().getX() + 1), (this.getPosition().getY()), this.getPosition().getOrientation()) :
                this.getPosition().getOrientation().equals(Orientation.S) ?
                new Position ((this.getPosition().getX()), (this.getPosition().getY() - 1), this.getPosition().getOrientation()) :
                this.getPosition().getOrientation().equals(Orientation.W) ?
                new Position ((this.getPosition().getX() - 1), (this.getPosition().getY()), this.getPosition().getOrientation()) :
                this.getPosition()
            );
        }
        else if (movement.equals (Movement.L)) {
            this.setPosition(
                this.getPosition().getX(),
                this.getPosition().getY(),
                (
                this.getPosition().getOrientation().equals(Orientation.N) ? Orientation.W :
                this.getPosition().getOrientation().equals(Orientation.W) ? Orientation.S :
                this.getPosition().getOrientation().equals(Orientation.S) ? Orientation.E :
                this.getPosition().getOrientation().equals(Orientation.E) ? Orientation.N :
                this.getPosition().getOrientation()
                )
            );
        }
        else if (movement.equals (Movement.R)) {
            this.setPosition(
                this.getPosition().getX(),
                this.getPosition().getY(),
                (
                this.getPosition().getOrientation().equals(Orientation.N) ? Orientation.E :
                this.getPosition().getOrientation().equals(Orientation.E) ? Orientation.S :
                this.getPosition().getOrientation().equals(Orientation.S) ? Orientation.W :
                this.getPosition().getOrientation().equals(Orientation.W) ? Orientation.N :
                this.getPosition().getOrientation()
                )
            );
        }

        if (this.getGrid().isValidPositionOnGrid(this.getPosition()) == false) {
            this.wouldHaveBeenPosition = this.getPosition();
            this.setPosition(null);
            this.getGrid().addScent(new Scent(lastKnownPosition, movement));
            throw new FellOffTheGridException("Fell off the grid");
        }
    }
}
