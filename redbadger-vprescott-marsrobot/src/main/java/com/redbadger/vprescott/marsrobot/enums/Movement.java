package com.redbadger.vprescott.marsrobot.enums;

public enum Movement {
    L, R, F
}