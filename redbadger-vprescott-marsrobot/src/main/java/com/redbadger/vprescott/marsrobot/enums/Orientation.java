package com.redbadger.vprescott.marsrobot.enums;

public enum Orientation {
    E, S, W, N
}
