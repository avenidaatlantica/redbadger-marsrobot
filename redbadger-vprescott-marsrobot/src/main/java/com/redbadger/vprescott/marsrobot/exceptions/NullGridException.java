package com.redbadger.vprescott.marsrobot.exceptions;

public class NullGridException extends Exception {

    public NullGridException(String message) {
        super(message);
    }

    public NullGridException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
