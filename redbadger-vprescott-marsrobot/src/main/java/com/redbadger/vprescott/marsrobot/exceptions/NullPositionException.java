package com.redbadger.vprescott.marsrobot.exceptions;

public class NullPositionException extends Exception {

    public NullPositionException(String message) {
        super(message);
    }

    public NullPositionException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
