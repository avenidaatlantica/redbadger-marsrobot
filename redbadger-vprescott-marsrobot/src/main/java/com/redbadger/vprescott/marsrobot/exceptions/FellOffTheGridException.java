package com.redbadger.vprescott.marsrobot.exceptions;

public class FellOffTheGridException extends Exception {

    public FellOffTheGridException(String message) {
        super(message);
    }

    public FellOffTheGridException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
