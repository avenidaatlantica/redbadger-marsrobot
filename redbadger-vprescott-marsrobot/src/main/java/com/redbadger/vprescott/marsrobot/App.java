package com.redbadger.vprescott.marsrobot;

import com.redbadger.vprescott.marsrobot.domain.Grid;
import com.redbadger.vprescott.marsrobot.domain.Robot;
import com.redbadger.vprescott.marsrobot.enums.Movement;
import com.redbadger.vprescott.marsrobot.exceptions.*;

import java.io.Console;
import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {

        Movement[] movements = Movement.values();
        Map<String,Boolean> validMovements = new HashMap<String,Boolean>();
        for (Movement movement : movements) {
            validMovements.put(movement.toString(), true);
        }

        Console c = System.console();
        Grid grid = null;
        Robot robot = null;

        while (true) {

            String gridDimensions = c.readLine("Please provide the grid dimensions (N N): ");
            String[] gridDimensionsArray = gridDimensions.split(" ");
            if (gridDimensionsArray.length != 2) {
                c.writer().print("Invalid input! ");
                continue;
            }

            int gridWidth;
            int gridHeight;

            try {
                gridWidth = Integer.parseInt(gridDimensionsArray[0]);
                gridHeight = Integer.parseInt(gridDimensionsArray[1]);
            } catch (NumberFormatException e) {
                c.writer().print("Invalid numbers! ");
                continue;
            }

            try {
                grid = new Grid(gridWidth, gridHeight);
            } catch (InvalidGridSizeException e) {
                c.writer().print("Invalid grid dimensions! ");
                continue;
            }

            break;
        }

        robot = new Robot();
        robot.setGrid(grid);

        c.writer().println (formatGridSize (grid));
        c.writer().println (formatRobotPosition (robot));

        while (true) {
            String robotMovementsString = c.readLine("Next robot movements: ");
            String[] robotMovements = robotMovementsString.split("");
            Map<String,Boolean> invalidMovements = new HashMap<String,Boolean>();
            for (int i=0; i<robotMovements.length; i++) {
                if (! validMovements.containsKey(robotMovements[i])) {
                    invalidMovements.put(robotMovements[i], true);
                }
            }
            if (invalidMovements.keySet().size() > 0) {
                c.writer().println("Invalid movements: " + formatInvalidMovements(invalidMovements));
                c.writer().println (formatRobotPosition (robot));
                continue;
            }

            for (int i=0; i<robotMovements.length; i++) {
                try {
                    robot.move(Movement.valueOf(robotMovements[i]));
                } catch (NullGridException e) {
                    // Should not happen
                    c.writer().println(e.getStackTrace());
                } catch (NullPositionException e) {
                    // Should not happen
                    c.writer().println(e.getStackTrace());
                } catch (FellOffTheGridException e) {
                    c.writer().println(formatRobotLostPosition(robot));
                    c.writer().println("This robot is dead by now, we are launching a new robot");
                    robot = new Robot();
                    robot.setGrid(grid);
                    break;
                } catch (WontFallWhereOthersFellBeforeException e) {
                    c.writer().println("The robot has refused to fall where others fell before, sorry!");
                    break;
                }
            }

            c.writer().println (formatRobotPosition (robot));
        }
    }

    private static String formatGridSize(Grid grid) {
        return (grid.getWidth() + " " + grid.getHeight());
    }

    private static String formatRobotPosition(Robot robot) {
        return (robot.getPosition().getX() + " " + robot.getPosition().getY() + " " + robot.getPosition().getOrientation());
    }

    private static String formatRobotLostPosition(Robot robot) {
        return (robot.getWouldHaveBeenPosition().getX() + " " + robot.getWouldHaveBeenPosition().getY() + " " + robot.getWouldHaveBeenPosition().getOrientation() + " LOST");
    }

    private static String formatInvalidMovements(Map invalidMovements) {
        return (String.join(", ", invalidMovements.keySet()));
    }

}
