package com.redbadger.vprescott.marsrobot.domain;

import com.redbadger.vprescott.marsrobot.enums.Movement;
import com.redbadger.vprescott.marsrobot.enums.Orientation;
import com.redbadger.vprescott.marsrobot.exceptions.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/beans.xml")
public class RobotTest {

    /*
    Test the normal initialisation of a Robot
    */

    @Test
    public void testRobot () throws InvalidGridSizeException {

        Robot robot = new Robot();

        assertTrue (robot.getGrid() == null);
        assertTrue (robot.getPosition() == null);

        Grid grid = mock(Grid.class);
        robot.setGrid(grid);
        assertTrue (robot.getGrid().equals(grid));
        assertTrue (robot.getPosition() != null);
    }

    /*
    Test that Robot can't be moved if he has got no Grid
    */

    @Test(expected = NullGridException.class)
    public void testRobotMoveNullGrid () throws NullGridException, NullPositionException, FellOffTheGridException, WontFallWhereOthersFellBeforeException {

        Robot robot = new Robot();
        assertTrue (robot.getGrid() == null);
        robot.move(Movement.F);
    }
}
