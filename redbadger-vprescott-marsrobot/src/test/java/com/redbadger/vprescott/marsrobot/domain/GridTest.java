package com.redbadger.vprescott.marsrobot.domain;

import com.redbadger.vprescott.marsrobot.enums.Movement;
import com.redbadger.vprescott.marsrobot.enums.Orientation;
import com.redbadger.vprescott.marsrobot.exceptions.InvalidGridSizeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/beans.xml")
public class GridTest {

    /*
    Test the normal initialisation of a Grid
    */

    @Test
    public void testGrid () throws InvalidGridSizeException {

        int width = 10;
        int height = 11;

        Grid grid = new Grid(width, height);

        assertTrue (grid.getWidth() == width);
        assertTrue (grid.getHeight() == height);

    }

    /*
    Test the initialisation of a Grid with invalid width (value too low)
    */

    @Test(expected = InvalidGridSizeException.class)
    public void testGridWidthTooLow () throws InvalidGridSizeException {

        int width = Grid.getMinWidth() - 1;
        int height = 10;

        Grid grid = new Grid(width, height);
    }

    /*
    Test the initialisation of a Grid with invalid width (value too high)
    */

    @Test(expected = InvalidGridSizeException.class)
    public void testGridWidthTooHigh () throws InvalidGridSizeException {

        int width = Grid.getMaxWidth() + 1;
        int height = 10;

        Grid grid = new Grid(width, height);
    }

    /*
    Test the initialisation of a Grid with invalid height (value too low)
    */

    @Test(expected = InvalidGridSizeException.class)
    public void testGridHeightTooLow () throws InvalidGridSizeException {

        int width = 10;
        int height = Grid.getMinHeight() - 1;

        Grid grid = new Grid(width, height);
    }

    /*
    Test the initialisation of a Grid with invalid height (value too high)
    */

    @Test(expected = InvalidGridSizeException.class)
    public void testGridHeightTooHigh () throws InvalidGridSizeException {

        int width = 10;
        int height = Grid.getMaxHeight() + 1;

        Grid grid = new Grid(width, height);
    }

    /*
    Test the valid and invalid positions on the Grid
    */

    @Test
    public void testGridPosition () throws InvalidGridSizeException {

        int width = 10;
        int height = 11;

        Grid grid = new Grid(width, height);

        Position position1 = new Position(width - 1, height - 1);
        assertTrue (grid.isValidPositionOnGrid(position1) == true);

        Position position2 = new Position(width + 1, height + 1);
        assertTrue (grid.isValidPositionOnGrid(position2) == false);

        Position position3 = new Position(-1, -1);
        assertTrue (grid.isValidPositionOnGrid(position3) == false);
    }

    /*
    Test scents
    */

    @Test
    public void testScents () throws InvalidGridSizeException {

        int width = 10;
        int height = 11;

        Grid grid = new Grid(width, height);

        Scent scent1 = new Scent(new Position (5, 6, Orientation.N), Movement.F);
        grid.addScent(scent1);
        assertTrue (grid.hasScentOnThisMove(new Position (5, 6, Orientation.N), Movement.F));
        assertFalse (grid.hasScentOnThisMove(new Position (6, 6, Orientation.N), Movement.F));
        assertFalse (grid.hasScentOnThisMove(new Position (5, 6, Orientation.N), Movement.L));

    }
}
