package com.redbadger.vprescott.marsrobot.acceptance;

import com.redbadger.vprescott.marsrobot.domain.Grid;
import com.redbadger.vprescott.marsrobot.domain.Position;
import com.redbadger.vprescott.marsrobot.domain.Robot;
import com.redbadger.vprescott.marsrobot.enums.Movement;
import com.redbadger.vprescott.marsrobot.enums.Orientation;
import com.redbadger.vprescott.marsrobot.exceptions.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/beans.xml")
public class AcceptanceTest {

    @Test
    public void acceptanceTest () throws InvalidGridSizeException,
            NullGridException,
            WontFallWhereOthersFellBeforeException,
            NullPositionException,
            FellOffTheGridException {

        Grid grid = new Grid (10, 10);
        Robot robot = new Robot();
        robot.setGrid(grid);
        assertTrue(robot.getPosition().equals(new Position(1, 1, Orientation.E)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(2, 1, Orientation.E)));

        robot.move(Movement.L);
        assertTrue(robot.getPosition().equals(new Position(2, 1, Orientation.N)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(2, 2, Orientation.N)));

        robot.move(Movement.L);
        assertTrue(robot.getPosition().equals(new Position(2, 2, Orientation.W)));

        robot.move(Movement.L);
        assertTrue(robot.getPosition().equals(new Position(2, 2, Orientation.S)));

        robot.move(Movement.L);
        assertTrue(robot.getPosition().equals(new Position(2, 2, Orientation.E)));

        robot.move(Movement.R);
        assertTrue(robot.getPosition().equals(new Position(2, 2, Orientation.S)));

        robot.move(Movement.R);
        assertTrue(robot.getPosition().equals(new Position(2, 2, Orientation.W)));

        robot.move(Movement.R);
        assertTrue(robot.getPosition().equals(new Position(2, 2, Orientation.N)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(2, 3, Orientation.N)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(2, 4, Orientation.N)));

        robot.move(Movement.R);
        assertTrue(robot.getPosition().equals(new Position(2, 4, Orientation.E)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(3, 4, Orientation.E)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(4, 4, Orientation.E)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(5, 4, Orientation.E)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(6, 4, Orientation.E)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(7, 4, Orientation.E)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(8, 4, Orientation.E)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(9, 4, Orientation.E)));

        robot.move(Movement.F);
        assertTrue(robot.getPosition().equals(new Position(10, 4, Orientation.E)));

        /*
        Now it is going to fall of the grid
         */

        FellOffTheGridException fellOffTheGridException = null;

        try {
            robot.move(Movement.F);
            }
        catch (FellOffTheGridException e) {
            fellOffTheGridException = e;
        }

        assertTrue(fellOffTheGridException != null);

        /*
        Now that it has fallen off the grid it can't go anywhere
         */

        NullPositionException nullPositionException = null;

        try {
            robot.move(Movement.F);
        }
        catch (NullPositionException e) {
            nullPositionException = e;
        }

        assertTrue(nullPositionException != null);

        /*
        Try another robot and get him to try to fall of the same location
         */

        Robot robot2 = new Robot();
        robot2.setGrid(grid);

        robot2.move(Movement.L);
        robot2.move(Movement.F);
        robot2.move(Movement.F);
        robot2.move(Movement.F);
        robot2.move(Movement.R);
        robot2.move(Movement.F);
        robot2.move(Movement.F);
        robot2.move(Movement.F);
        robot2.move(Movement.F);
        robot2.move(Movement.F);
        robot2.move(Movement.F);
        robot2.move(Movement.F);
        robot2.move(Movement.F);
        robot2.move(Movement.F);
        assertTrue(robot2.getPosition().equals(new Position(10, 4, Orientation.E)));

        /*
        Check that he will not attempt to fall off the same location where another one fell
         */

        WontFallWhereOthersFellBeforeException wontFallWhereOthersFellBeforeException = null;

        try {
            robot2.move(Movement.F);
        }
        catch (WontFallWhereOthersFellBeforeException e) {
            wontFallWhereOthersFellBeforeException = e;
        }

        assertTrue(wontFallWhereOthersFellBeforeException != null);

    }
}
