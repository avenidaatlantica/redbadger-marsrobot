package com.redbadger.vprescott.marsrobot.domain;

import com.redbadger.vprescott.marsrobot.enums.Orientation;
import com.redbadger.vprescott.marsrobot.exceptions.InvalidGridSizeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/beans.xml")
public class PositionTest {

    /*
    Test the normal initialisation of a Position
    */

    @Test
    public void testPosition () {

        int x = 10;
        int y = 11;
        Orientation orientation = Orientation.W;

        Position position1 = new Position(x, y, orientation);
        assertTrue (position1.getX() == x);
        assertTrue (position1.getY() == y);
        assertTrue (position1.getOrientation().equals(orientation));

        Position position2 = new Position(x, y);
        assertTrue (position2.getX() == x);
        assertTrue (position2.getY() == y);
        assertTrue (position2.getOrientation() != null);

        Position position3 = new Position();
        assertTrue (position3.getX() > 0);
        assertTrue (position3.getY() > 0);
        assertTrue (position3.getOrientation() != null);
    }

}
