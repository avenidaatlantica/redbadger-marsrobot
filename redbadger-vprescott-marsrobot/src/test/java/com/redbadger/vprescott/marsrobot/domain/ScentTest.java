package com.redbadger.vprescott.marsrobot.domain;

import com.redbadger.vprescott.marsrobot.enums.Movement;
import com.redbadger.vprescott.marsrobot.enums.Orientation;
import com.redbadger.vprescott.marsrobot.exceptions.InvalidGridSizeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/beans.xml")
public class ScentTest {

    /*
    Test the normal initialisation of a Scent
    */

    @Test
    public void testScent () {

        Position position = new Position (10, 1, Orientation.N);
        Movement movement = Movement.F;

        Scent scent = new Scent(position, movement);

        assertTrue (scent.getPosition().equals(position));
        assertTrue (scent.getMovement().equals(movement));
    }
}
